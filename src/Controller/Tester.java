package Controller;

import java.util.ArrayList;

import Interface.Measurable;
import Interface.Taxable;
import Model.BankAccount;
import Model.Company;
import Model.Country;
import Model.Data;
import Model.Person;
import Model.Product;
import Model.TaxCalculator;

public class Tester {

	public static void main(String[] args) {
		Tester tester = new Tester();
		tester.testPerson();
		tester.testMin();
		tester.testTax();
	}

	public void testPerson() {
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("Rick", 185);
		persons[1] = new Person("Glenn", 180);
		persons[2] = new Person("Carl", 175);
		double averageHeight = Data.average(persons);

		System.out.println("Test Person");
		System.out.println("Average height: " + averageHeight);
		System.out.println("Expected: 180.0");
		System.out.println("-----------------------------------");
	}

	public void testMin() {
		Measurable[] persons = new Measurable[2];
		persons[0] = new Person("Rick", 185);
		persons[1] = new Person("Glenn", 180);
		
		Measurable[] bankAccounts = new Measurable[2];
		bankAccounts[0] = new BankAccount("Rick", 2000);
		bankAccounts[1] = new BankAccount("Glenn", 4000);
		
		Measurable[] countries = new Measurable[2];
		countries[0] = new Country("USA", 318900000);
		countries[1] = new Country("Thailand", 67000000);
		
		System.out.println("Test Min");
		Person person = (Person) Data.min(persons[0], persons[1]);
		System.out.println("1. "+((Person) persons[0]).getName() + " " + persons[0].getMeasure());
		System.out.println("2. "+((Person) persons[1]).getName() + " " + persons[1].getMeasure());
		System.out.println("Min height: " + person.getName()+" "+person.getHeight() + "\n");
		
		BankAccount bankAccount = (BankAccount) Data.min(bankAccounts[0], bankAccounts[1]);
		System.out.println("1. "+((BankAccount) bankAccounts[0]).getName() + " " + bankAccounts[0].getMeasure());
		System.out.println("2. "+((BankAccount) bankAccounts[1]).getName() + " " + bankAccounts[1].getMeasure());
		System.out.println("Min balance: " + bankAccount.getName()+" "+bankAccount.getBalance() + "\n");
		
		Country country = (Country) Data.min(countries[0], countries[1]);
		System.out.println("1. "+((Country) countries[0]).getName() + " " + countries[0].getMeasure());
		System.out.println("2. "+((Country) countries[1]).getName() + " " + countries[1].getMeasure());
		System.out.println("Min population: " + country.getName()+" "+country.getPopulation());
		System.out.println("-----------------------------------");
	}
	public void testTax(){
		ArrayList<Taxable> persons = new ArrayList<Taxable>();
		persons.add(new Person("Rick", 185, 100000));
		persons.add(new Person("Glenn", 180, 500000));
		
		ArrayList<Taxable> companies = new ArrayList<Taxable>();
		companies.add(new Company("Apple", 1000000, 800000));
		companies.add(new Company("Windows", 2000000, 500000));
		
		ArrayList<Taxable> products = new ArrayList<Taxable>();
		products.add(new Product("TV", 10000));
		products.add(new Product("Computer", 20000));
		
		ArrayList<Taxable> allElements = new ArrayList<Taxable>();
		allElements.addAll(persons);
		allElements.addAll(companies);
		allElements.addAll(products);
		
		System.out.println("Test Tax");
		
		for(Taxable person:persons){
			Person p = (Person)person;
			System.out.println(p.getName()+" income:"+p.getYearlyIncome());
		}
		System.out.println("Tax sum persons: "+ TaxCalculator.sum(persons));
		System.out.println("Expected: 5000+35000=40000\n");
		
		for(Taxable company:companies){
			Company c = (Company) company;
			System.out.println(c.getName()+" income:"+c.getIncome()+" expenses:" + c.getExpenses());
		}
		System.out.println("Tax sum companies: "+ TaxCalculator.sum(companies));
		System.out.println("Expected: " + (200000*0.3)+"+"+(1500000*0.3) +" = "+510000+ "\n");
		
		for(Taxable product:products){
			Product p = (Product) product;
			System.out.println(p.getName()+" income:"+p.getPrice());
		}
		System.out.println("Tax sum products: "+ TaxCalculator.sum(products));
		System.out.println("Expected: 700+1400 = 2100 \n");
		
		System.out.println("Tax sum all elements: "+ TaxCalculator.sum(allElements));
		System.out.println("Expected: 40000+510000+2100 = 552100");
		System.out.println("-----------------------------------");
	}
}
